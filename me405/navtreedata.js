/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405", "index.html", [
    [ "Introduction & Links", "index.html#sec_intro", null ],
    [ "HW 0x00: State Transition Diagrams", "pg_hw0.html", null ],
    [ "HW 0x01: Python Review (Functions/Tuples)", "pg_hw1.html", null ],
    [ "Lab 0x01: Reintroduction to Python and Finite State Machines", "pg_lab1.html", null ],
    [ "Lab 0x02: Think Fast!", "pg_lab2.html", null ],
    [ "Lab 0x03: Pushing the Right Buttons", "pg_lab3.html", null ],
    [ "Lab 0x04: Hot or Not?", "pg_lab4.html", null ],
    [ "Term Project: Two Axis Balancing Platform", "term_proj.html", null ],
    [ "BNO055 IMU Driver", "imu_driver.html", null ],
    [ "Encoder & Motor Drivers", "enc_m_drivers.html", null ],
    [ "Fun with Filters", "signal_filtering.html", null ],
    [ "System Model Testing", "term_proj_model_testing.html", null ],
    [ "Resistive Touch Pannel Driver", "rtp_driver_testing.html", null ],
    [ "Final Intergration", "final_int.html", null ],
    [ "System Modeling", "term_proj_modeling.html", null ],
    [ "MATLAB System Modeling", "term_proj_modeling_MATLAB.html", null ],
    [ "System Modeling (First Attempt)", "term_proj_modelingR1.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", "namespacemembers_vars" ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Filter_01Coef_01Format_2coeff__format_8py.html",
"classbno055__base_1_1BNO055__BASE.html#a946856083839b9b5283f2a07ece000f9",
"lpf_8py.html",
"system__ctrl__design_8py.html#a40f3a2876024b216b9dec14260823af6"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';