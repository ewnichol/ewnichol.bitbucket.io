var me405_01Lab_010x04_2main_8py =
[
    [ "adc", "me405_01Lab_010x04_2main_8py.html#afac357daa82cb1f4a2ca28df4f60bb14", null ],
    [ "curr_time", "me405_01Lab_010x04_2main_8py.html#af0fea2d37afc938ca5ccd6bc7198165f", null ],
    [ "i2c", "me405_01Lab_010x04_2main_8py.html#ab2bba10cbf0946121616503df3750664", null ],
    [ "mcp", "me405_01Lab_010x04_2main_8py.html#a0ce8364e33e8481fd92c82a93e8d8392", null ],
    [ "start_time", "me405_01Lab_010x04_2main_8py.html#ae57958345b17f9ca8597330ba07e1a1c", null ],
    [ "T_amb_C", "me405_01Lab_010x04_2main_8py.html#a066b34b01ea2377930886aa34efda7d2", null ],
    [ "T_amb_F", "me405_01Lab_010x04_2main_8py.html#a9e0ea8708de369491014e6bfbf21a22e", null ],
    [ "T_core_C", "me405_01Lab_010x04_2main_8py.html#ad81daf3c58622c3f2b26989696a0b6a0", null ],
    [ "T_core_F", "me405_01Lab_010x04_2main_8py.html#aadd208dd077501de0a3e2dcc5de95f95", null ],
    [ "time", "me405_01Lab_010x04_2main_8py.html#afd3f627b76216c351ca3653d5a60445c", null ]
];