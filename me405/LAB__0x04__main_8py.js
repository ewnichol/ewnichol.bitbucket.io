var LAB__0x04__main_8py =
[
    [ "user_button_pressed", "LAB__0x04__main_8py.html#a207ac3277ba476c284e3bf9eb5744449", null ],
    [ "amb_temp", "LAB__0x04__main_8py.html#a13e4cff6e320564158c248f31a4493e9", null ],
    [ "board_LED", "LAB__0x04__main_8py.html#a7b113242b0b3672b046b833eb30e415c", null ],
    [ "core_sensor", "LAB__0x04__main_8py.html#a0797aee48d3c6de3709527717ee42e74", null ],
    [ "core_temp", "LAB__0x04__main_8py.html#aaaa966e93d8f493ac86909402db26704", null ],
    [ "cur_time", "LAB__0x04__main_8py.html#a10f6ecfbed756c301c1187fb5bd78fc3", null ],
    [ "el_time", "LAB__0x04__main_8py.html#a361d46270cc5fc9e3611b6d6d5e4079d", null ],
    [ "extint", "LAB__0x04__main_8py.html#a2beff0ab22354a77a99b58060df0f018", null ],
    [ "i2c", "LAB__0x04__main_8py.html#a896765343abe1a9c80f65484676ae986", null ],
    [ "PRESS", "LAB__0x04__main_8py.html#abeb067894ddcde23c8c9fc760f6bbf63", null ],
    [ "Press", "LAB__0x04__main_8py.html#a4cb38990798c2c8bc46a0dc2dc6aa1a5", null ],
    [ "start_time", "LAB__0x04__main_8py.html#a9d114c642e10c3f6ecb54c89fbafe54c", null ],
    [ "temp_sensor", "LAB__0x04__main_8py.html#aeaa15f4b206216856373b17fb7941f21", null ],
    [ "user_button", "LAB__0x04__main_8py.html#ae2b4b5d7dee856b06d2c9efa282475f5", null ],
    [ "vref", "LAB__0x04__main_8py.html#a47e16d7d1bb922eb658ef015bd8b4c48", null ]
];