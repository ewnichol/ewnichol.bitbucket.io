var searchData=
[
  ['r_327',['r',['../namespacecoeff__format.html#aa2f419f9c76d68eb49453a9395ced7b6',1,'coeff_format.r()'],['../namespacebalance.html#a9b417da12679ce58a56b38951edab20d',1,'balance.R()']]],
  ['r1_5fconfig_328',['R1_CONFIG',['../classmcp9808_1_1MCP9808.html#ae0016f4aced49151292695f53f934171',1,'mcp9808::MCP9808']]],
  ['r2_5fupper_5ftemp_329',['R2_UPPER_TEMP',['../classmcp9808_1_1MCP9808.html#a982f7a4706c44a211eee64701feff843',1,'mcp9808::MCP9808']]],
  ['r3_5flower_5ftemp_330',['R3_LOWER_TEMP',['../classmcp9808_1_1MCP9808.html#a3034fa84f6b89c6aeb7e668c2c65872f',1,'mcp9808::MCP9808']]],
  ['r4_5fcrit_5ftemp_331',['R4_CRIT_TEMP',['../classmcp9808_1_1MCP9808.html#a9595210e1f925b78f4cca46299d2d80f',1,'mcp9808::MCP9808']]],
  ['r5_5ftemperature_332',['R5_TEMPERATURE',['../classmcp9808_1_1MCP9808.html#a0d0fc5e7469ddc886f0b7b391cf803e4',1,'mcp9808::MCP9808']]],
  ['r6_5fmfg_5fid_333',['R6_MFG_ID',['../classmcp9808_1_1MCP9808.html#a0372139a5f678d008235a8250e506de3',1,'mcp9808::MCP9808']]],
  ['r7_5fdev_5fid_5fand_5frev_334',['R7_DEV_ID_AND_REV',['../classmcp9808_1_1MCP9808.html#a139f7dc0ab951bbabd4754709427bacb',1,'mcp9808::MCP9808']]],
  ['r8_5fresolution_335',['R8_RESOLUTION',['../classmcp9808_1_1MCP9808.html#a2849568c6f32a4a18ea12a8282e12a3b',1,'mcp9808::MCP9808']]],
  ['r_5fplat_336',['R_plat',['../namespacesystem__ctrl__design.html#accef70bf61280d433aeb1d242815cef1',1,'system_ctrl_design']]],
  ['r_5fplat_5fcl0_337',['R_plat_CL0',['../namespacesystem__ctrl__design.html#a2f487049aa22a33859f431e2bd2d28b2',1,'system_ctrl_design']]],
  ['r_5fplat_5fcl1_338',['R_plat_CL1',['../namespacesystem__ctrl__design.html#a358a5d7396a6b2223ce12cf5917d38be',1,'system_ctrl_design']]],
  ['r_5fsys_339',['R_sys',['../namespacesystem__ctrl__design.html#a6fc0d3027868f82e0296e753e9d958cf',1,'system_ctrl_design']]],
  ['r_5fsys_5fcl0_340',['R_sys_CL0',['../namespacesystem__ctrl__design.html#ae8f2cfc1cf1b83a6d7ae52fe1046c44b',1,'system_ctrl_design']]],
  ['r_5fsys_5fcl1_341',['R_sys_CL1',['../namespacesystem__ctrl__design.html#aafdbf63ea18435f9e74f8a4ed134d327',1,'system_ctrl_design']]],
  ['ra_342',['RA',['../namespacesystem__modeling.html#a22869ac670ada45f6368b57900e38ebb',1,'system_modeling']]],
  ['ra_5fcl_343',['RA_cl',['../namespacesystem__modeling.html#aba64f90903ec4ab3f470eeb4dceae4ec',1,'system_modeling']]],
  ['rate_344',['rate',['../namespaceLAB__0x04__I2C__TEST.html#a777907083de4ef77a5b823f1d9cfd991',1,'LAB_0x04_I2C_TEST']]],
  ['rb_345',['RB',['../namespacesystem__modeling.html#a2bbe475eb530d629858b5fa3e28bf532',1,'system_modeling']]],
  ['rb_5fcl_346',['RB_cl',['../namespacesystem__modeling.html#acf0c776006f0cf52880503d57ef4d2a6',1,'system_modeling']]],
  ['rbit_347',['rbit',['../namespaceasm.html#a6e7ee449f8abe38381ae89b40ceb0e3a',1,'asm']]],
  ['rbuflen_348',['RBUFLEN',['../namespacecorrelate.html#a7fcc4ea1278157624f3a500b1bc932e0',1,'correlate.RBUFLEN()'],['../namespacefilt__test.html#aeb4a467105427e09d970001104d08919',1,'filt_test.RBUFLEN()']]],
  ['rc_349',['RC',['../namespacesystem__modeling.html#a2febe0c19c5c9b67cd9ab1c7ccaeeddd',1,'system_modeling']]],
  ['rc_5fcl_350',['RC_cl',['../namespacesystem__modeling.html#a374391d000c4bb5ed5711da3b5d7f441',1,'system_modeling']]],
  ['rd_351',['RD',['../namespacesystem__modeling.html#a4e0702d0c6e1255bc0b7c2740bce70ef',1,'system_modeling']]],
  ['reaction_5flog_352',['reaction_log',['../namespaceLAB__0x02__ThinkFast.html#a4724b7e27a99a63087346a25f98bc4c0',1,'LAB_0x02_ThinkFast']]],
  ['reaction_5fnumber_353',['reaction_number',['../namespaceLAB__0x02__ThinkFast.html#a2ba96e0303463f1673b0900ae893834f',1,'LAB_0x02_ThinkFast']]],
  ['read_5fbuf_354',['read_buf',['../classmcp9808_1_1MCP9808.html#a42c40c0457f497628feb984ee26d828d',1,'mcp9808::MCP9808']]],
  ['recieve_355',['recieve',['../classLAB__0x03__UI__FrontEnd_1_1FrontEnd__UIFSM.html#ac35d2b61a0f04a836b2682ec412ecb9f',1,'LAB_0x03_UI_FrontEnd::FrontEnd_UIFSM']]],
  ['res_356',['res',['../classmcp9808_1_1MCP9808.html#a5ea5fbfa9edbd41675421884079e7119',1,'mcp9808.MCP9808.res()'],['../classmcp9808_1_1MCP9808.html#ac86175e12f274452a609d16bdc943a83',1,'mcp9808.MCP9808.res(self, res=&apos;GET&apos;)'],['../namespacecorrelate.html#a02cd809d1c7d5cc24c4b4552ba8ad702',1,'correlate.res()']]],
  ['reset_357',['reset',['../classbno055__base_1_1BNO055__BASE.html#a7b14453c7a3e6d3143632807bf689bd8',1,'bno055_base::BNO055_BASE']]],
  ['reset_5fdata_358',['reset_data',['../classLAB__0x03__UI__BackEnd_1_1BackEnd__FSM.html#a280af98b417aa0100df69376cfb3f0e9',1,'LAB_0x03_UI_BackEnd::BackEnd_FSM']]],
  ['reverse_359',['REVERSE',['../namespacefilt.html#a3467f1033e0f6cb95f313dd58a96c510',1,'filt']]],
  ['rn_5fanalog_360',['rn_analog',['../namespacecorrelate.html#ac32069242807cb11633421a4674325a9',1,'correlate']]],
  ['rtp_361',['RTP',['../classrtp__driver_1_1RTP.html',1,'rtp_driver.RTP'],['../namespacebalance.html#a69fb5fd1b7af8aec7780359e5e3eaf44',1,'balance.RTP()'],['../namespacemain.html#a783171069fcb818c2f1c911d0da600df',1,'main.RTP()'],['../namespacertp__driver.html#addf9ab7799b382411add694aaf7b138d',1,'rtp_driver.RTP()'],['../namespacertp__filter__testing.html#ad8c756286066f0695b718a3b88b66e33',1,'rtp_filter_testing.RTP()']]],
  ['rtp_5fdriver_362',['rtp_driver',['../namespacertp__driver.html',1,'']]],
  ['rtp_5fdriver_2epy_363',['rtp_driver.py',['../rtp__driver_8py.html',1,'']]],
  ['resistive_20touch_20pannel_20driver_364',['Resistive Touch Pannel Driver',['../rtp_driver_testing.html',1,'']]],
  ['rtp_5ffilter_5ftesting_365',['rtp_filter_testing',['../namespacertp__filter__testing.html',1,'']]],
  ['rtp_5ffilter_5ftesting_2epy_366',['rtp_filter_testing.py',['../rtp__filter__testing_8py.html',1,'']]],
  ['run_367',['run',['../classLAB__0x03__UI__BackEnd_1_1BackEnd__FSM.html#a8e3b2d2295ad2b7824f63de37d13fb60',1,'LAB_0x03_UI_BackEnd.BackEnd_FSM.run()'],['../classLAB__0x03__UI__FrontEnd_1_1FrontEnd__UIFSM.html#a74743538bfe8ac7134b265fb04a2cd6e',1,'LAB_0x03_UI_FrontEnd.FrontEnd_UIFSM.run()']]],
  ['run_5ftime_368',['run_time',['../namespaceenc__driver.html#af59f916f18d10e2d249c3b9f643e2307',1,'enc_driver.run_time()'],['../namespacem__driver.html#a1cab8bef092f6fda08a071b02426f4cb',1,'m_driver.run_time()'],['../namespacertp__driver.html#a36e667fba059b9a423170381b74eeada',1,'rtp_driver.run_time()']]],
  ['runlength_369',['runlength',['../namespacesamples.html#a54fa8d5e94bd79d021df9d25372df1e3',1,'samples']]],
  ['runs_370',['runs',['../namespacebalance.html#aa6cf9f76d325b71863ebb7a8410e2b61',1,'balance']]]
];
