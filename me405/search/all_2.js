var searchData=
[
  ['b_37',['B',['../namespacesystem__ctrl__design.html#a70a562d016dc12dfd504be1fcb43a4f4',1,'system_ctrl_design.B()'],['../namespacesystem__modeling.html#a43d5637f5047407d505e60f9218b55e5',1,'system_modeling.B()']]],
  ['b_5fcl_38',['B_cl',['../namespacesystem__modeling.html#ae8fac31f3b8a13686fd65d48668757ac',1,'system_modeling']]],
  ['b_5fp_39',['B_p',['../namespacesystem__ctrl__design.html#a40f3a2876024b216b9dec14260823af6',1,'system_ctrl_design']]],
  ['backend_5ffsm_40',['BackEnd_FSM',['../classLAB__0x03__UI__BackEnd_1_1BackEnd__FSM.html',1,'LAB_0x03_UI_BackEnd']]],
  ['bal_5flim_41',['bal_lim',['../namespacebalance.html#a484727b14cc95b9440e32fb05def7d8b',1,'balance']]],
  ['balance_42',['balance',['../namespacebalance.html',1,'balance'],['../namespaceLAB__0x01__Vendotron.html#acd7f2b9d1bd86852b499116f0fb68ecc',1,'LAB_0x01_Vendotron.balance()']]],
  ['balance_2epy_43',['balance.py',['../balance_8py.html',1,'']]],
  ['baudrate_44',['baudrate',['../namespaceLAB__0x04__I2C__TEST.html#a13fbbaf8a104ca6581b71f51c3a1b955',1,'LAB_0x04_I2C_TEST']]],
  ['bno055_45',['BNO055',['../classbno055_1_1BNO055.html',1,'bno055.BNO055'],['../namespacebno055.html',1,'bno055']]],
  ['bno055_2epy_46',['bno055.py',['../bno055_8py.html',1,'']]],
  ['bno055_5fbase_47',['BNO055_BASE',['../classbno055__base_1_1BNO055__BASE.html',1,'bno055_base.BNO055_BASE'],['../namespacebno055__base.html',1,'bno055_base']]],
  ['bno055_5fbase_2epy_48',['bno055_base.py',['../bno055__base_8py.html',1,'']]],
  ['bno055_5ftest_49',['bno055_test',['../namespacebno055__test.html',1,'']]],
  ['bno055_5ftest_2epy_50',['bno055_test.py',['../bno055__test_8py.html',1,'']]],
  ['board_5fled_51',['board_LED',['../namespaceLAB__0x02__ThinkFast.html#af38a4a3afeba14a99e4d9ac097cd4180',1,'LAB_0x02_ThinkFast.board_LED()'],['../namespaceLAB__0x04__main.html#a7b113242b0b3672b046b833eb30e415c',1,'LAB_0x04_main.board_LED()']]],
  ['buf1_52',['buf1',['../classLAB__0x03__UI__BackEnd_1_1BackEnd__FSM.html#abf42f4d1290d040dab2483b3e1de681b',1,'LAB_0x03_UI_BackEnd.BackEnd_FSM.buf1()'],['../namespaceLAB__0x03__UI__BackEnd.html#ad753b36e6b9b2201bc1281e323ff3412',1,'LAB_0x03_UI_BackEnd.buf1()']]],
  ['buf6_53',['buf6',['../classbno055_1_1BNO055.html#a07e0ebd05cb9d054d3e4cc7e9b9fb3ab',1,'bno055::BNO055']]],
  ['buf8_54',['buf8',['../classbno055_1_1BNO055.html#a30fa24fea34b4e56355c568972694146',1,'bno055::BNO055']]],
  ['bufin_55',['bufin',['../namespacecorrelate.html#ad8a68c6093311b87184baab74f1c86d1',1,'correlate.bufin()'],['../namespacefilt__test.html#a4ec5aa47a5e59615a5326eae48782689',1,'filt_test.bufin()']]],
  ['bno055_20imu_20driver_56',['BNO055 IMU Driver',['../imu_driver.html',1,'']]]
];
