var lpf_8py =
[
    [ "cb", "lpf_8py.html#a2b2220c7dac17606ac87f8216820c98e", null ],
    [ "sine_sweep", "lpf_8py.html#a2630fe71b5b8107c39e13441683ebffc", null ],
    [ "adc", "lpf_8py.html#a1a79f7b723019906766175a965163c1e", null ],
    [ "coeffs", "lpf_8py.html#a9bb7fbfcb64c0b49752475df6a9af65d", null ],
    [ "dac1", "lpf_8py.html#ab398d109c0f862e503418529ff4f43c0", null ],
    [ "dac2", "lpf_8py.html#a6283c38b29835caba03b0ce7896c8b36", null ],
    [ "data", "lpf_8py.html#a85bea8834bc00a7529dc518287a3bd42", null ],
    [ "ncoeffs", "lpf_8py.html#a8ac87afbc871dab1bcf29f8de2e91c78", null ],
    [ "tim", "lpf_8py.html#a1c34852fab73f437f2266a3b5a264f4e", null ]
];