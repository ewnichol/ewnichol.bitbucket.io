var LAB__0x02__ThinkFast_8py =
[
    [ "user_button_pressed", "LAB__0x02__ThinkFast_8py.html#a260849f9c2832bd60b220c0b23b136ae", null ],
    [ "avg_resp_time", "LAB__0x02__ThinkFast_8py.html#aa1891ec0066898dcbd6632ebe3a08fd6", null ],
    [ "board_LED", "LAB__0x02__ThinkFast_8py.html#af38a4a3afeba14a99e4d9ac097cd4180", null ],
    [ "delay_time", "LAB__0x02__ThinkFast_8py.html#a57bbd4b7e1ceb560bf436fc3caa397b3", null ],
    [ "extint", "LAB__0x02__ThinkFast_8py.html#a20b2b4eefd74d2a95eaf73515991e4c8", null ],
    [ "press_early", "LAB__0x02__ThinkFast_8py.html#a11eb3f697617eb066f810ee94e8ad897", null ],
    [ "press_num", "LAB__0x02__ThinkFast_8py.html#aa3bd9e6e4ca00e6859eff8c9aeeb9a58", null ],
    [ "press_time", "LAB__0x02__ThinkFast_8py.html#af60d026d758dfd3d30801b8a1f73ba2b", null ],
    [ "reaction_log", "LAB__0x02__ThinkFast_8py.html#a4724b7e27a99a63087346a25f98bc4c0", null ],
    [ "reaction_number", "LAB__0x02__ThinkFast_8py.html#a2ba96e0303463f1673b0900ae893834f", null ],
    [ "start_time", "LAB__0x02__ThinkFast_8py.html#a4c70efb66a615a732d13a358d57f14f0", null ],
    [ "tim", "LAB__0x02__ThinkFast_8py.html#a640867d4b4c1d422787e2350f72f6e87", null ],
    [ "user_button", "LAB__0x02__ThinkFast_8py.html#a03cc02979f3e73e3fcbabdc45e3cf61f", null ]
];