var classcl__driver_1_1ClosedLoop =
[
    [ "__init__", "classcl__driver_1_1ClosedLoop.html#aa267150095614c1b257071a2c5228976", null ],
    [ "compute", "classcl__driver_1_1ClosedLoop.html#a7abe15a4644d949b834d94774ecb79e4", null ],
    [ "get_KI", "classcl__driver_1_1ClosedLoop.html#adcb396fcdb5f539e01c453056533743f", null ],
    [ "get_KP", "classcl__driver_1_1ClosedLoop.html#a975a5466079b56982e38269e41109df4", null ],
    [ "reset", "classcl__driver_1_1ClosedLoop.html#aad64295b30e28823e6827059f6e27520", null ],
    [ "set_KI", "classcl__driver_1_1ClosedLoop.html#a758aa3f06006d18dcb79e17f640da5cb", null ],
    [ "set_KP", "classcl__driver_1_1ClosedLoop.html#ace0b854a76a86b12a6b8e15885f7ab29", null ],
    [ "ctrl_signal", "classcl__driver_1_1ClosedLoop.html#a206671cb7100f8ad3500fba77e8f9930", null ],
    [ "error", "classcl__driver_1_1ClosedLoop.html#ae6d9baa220d68dd3dbb71af436013574", null ],
    [ "ID", "classcl__driver_1_1ClosedLoop.html#a740a929996c89973394eee1a5de20e88", null ],
    [ "integral_error", "classcl__driver_1_1ClosedLoop.html#ae9b98e2c9f8d0e3f416e4cdbec1eb6da", null ],
    [ "KI", "classcl__driver_1_1ClosedLoop.html#a990c85c08d261eb5b5bfbe58871a48f9", null ],
    [ "KP", "classcl__driver_1_1ClosedLoop.html#a4a5c5781a8341ee908e4c3f43bc70383", null ],
    [ "meas", "classcl__driver_1_1ClosedLoop.html#a4c55a32675fdaf1f1b75cccf763f9a97", null ],
    [ "prev_error", "classcl__driver_1_1ClosedLoop.html#a0de3679e7fc5c64ecec1016fd3de5dbf", null ],
    [ "ref", "classcl__driver_1_1ClosedLoop.html#aa6d0bae98c6db7cb89f958450ee5c9da", null ],
    [ "sat_lim", "classcl__driver_1_1ClosedLoop.html#a37cc124bf74510dbe0d290692bde7659", null ]
];